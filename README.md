# Ambient Occlusion

Ambient Occlusion is a shading and rendering technique used to calculate how exposed each point in a scene is to ambient lighting.

All Implemented techniques are special for **Real-Time Rendering**, I will consider **Baked Ambient Occlusion** further when the project is mature enough.

# Motivation
This Project is my first serious Graphics Technique implemetation.

I  hope to both **Learn** a lot from this project and **Teach** what I learned as an interactive tutorial in my [blog](https://graphicscoder.com).

[TheForge](https://github.com/ConfettiFX/The-Forge) Graphics API was chosen as a first implementation to contribute to the open-source API as a unit-test (if it's worthy 	😃)

# Techniques
  - **Screen Space Algorithms**
    - [ ] SSAO
    

# Build
- **Windows**
  - [ ] VisualStudio 2019 (MSVC14)
- **Linux**
  - [ ] GCC
  
# Blog 
You can here about implementation details soon in my blog, [GraphicsCoder](https://graphicscoder.com)

# Third-Party Libraries
  - [TheForge](https://github.com/ConfettiFX/The-Forge)

# Resources
- SSAO
  - [LearnOpenGL SSAO](https://learnopengl.com/Advanced-Lighting/SSAO)
